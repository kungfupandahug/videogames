package com.games.Gamification.repositories;

import com.games.Gamification.models.CharacterDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterDetailRepository extends JpaRepository<CharacterDetail, Long> {

}
