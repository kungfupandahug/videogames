package com.games.Gamification.repositories;

import com.games.Gamification.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
