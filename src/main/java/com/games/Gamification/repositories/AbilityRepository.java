package com.games.Gamification.repositories;

import com.games.Gamification.models.Ability;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbilityRepository extends JpaRepository<Ability, Long> {

}
