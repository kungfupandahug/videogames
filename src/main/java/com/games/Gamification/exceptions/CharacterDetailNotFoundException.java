package com.games.Gamification.exceptions;

public class CharacterDetailNotFoundException extends RuntimeException {
    public CharacterDetailNotFoundException(Long id) {
        super("Could not find details about character with ID=" + id);
    }
}
