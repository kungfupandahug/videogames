package com.games.Gamification.exceptions;

public class AbilityNotFoundException extends RuntimeException {
    public AbilityNotFoundException(String className) {
        super("Could not find ability with name=" + className);
    }
}
