package com.games.Gamification.controllers;

import com.games.Gamification.components.AbilityResourceAssembler;
import com.games.Gamification.exceptions.AbilityNotFoundException;
import com.games.Gamification.models.Ability;
import com.games.Gamification.repositories.AbilityRepository;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class AbilityController {

    private final AbilityRepository abilityRepository;

    private final AbilityResourceAssembler assembler;

    public AbilityController(AbilityRepository abilityRepository,
                             AbilityResourceAssembler assembler) {
        this.abilityRepository = abilityRepository;
        this.assembler = assembler;
    }


    /*
    @GetMapping("/class/{className}")
    public Resource<?> one(@PathVariable String className) {

        Ability ability = abilityRepository.findById(id)
                .orElseThrow(() -> new AbilityNotFoundException(id));

        return assembler.toResource(ability);
    }
     */


    @GetMapping("/class/{className}")
    public Resource<?> one(@PathVariable String className) {

        List<Ability> abilities = abilityRepository.findAll()
                .stream()
                .filter(ability -> ability.getName().equals(className))
                .collect(Collectors.toList());

        Ability ability = abilities.get(0);
        if (ability == null)
            throw new AbilityNotFoundException(className);

        return assembler.toResource(ability);
    }


}
