package com.games.Gamification.controllers;

import com.games.Gamification.components.UserResourceAssembler;
import com.games.Gamification.exceptions.UserNotFoundException;
import com.games.Gamification.models.User;
import com.games.Gamification.repositories.UserRepository;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {

    private final UserRepository userRepository;

    private final UserResourceAssembler assembler;

    public UserController(UserRepository userRepository,
                          UserResourceAssembler assembler) {
        this.userRepository = userRepository;
        this.assembler = assembler;
    }

    @GetMapping("/users")
    public Resources<Resource<User>> all() {

        List<Resource<User>> users = userRepository.findAll()
                .stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(users,
                linkTo(methodOn(UserController.class).all()).withSelfRel());
    }


    @GetMapping("/user/{id}")
    public Resource<User> one(@PathVariable Long id) {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));

        return assembler.toResource(user); // TODO: right?
    }

    @PostMapping("/user")
    public ResponseEntity<?> newUser(@RequestBody User newUser) throws URISyntaxException {

        Resource<User> resource = assembler.toResource(userRepository.save(newUser));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);

    }




}
