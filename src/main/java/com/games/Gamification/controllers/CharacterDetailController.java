package com.games.Gamification.controllers;

import com.games.Gamification.Level;
import com.games.Gamification.components.CharacterDetailResourceAssembler;
import com.games.Gamification.components.UserResourceAssembler;
import com.games.Gamification.exceptions.CharacterDetailNotFoundException;
import com.games.Gamification.models.CharacterDetail;
import com.games.Gamification.repositories.CharacterDetailRepository;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class CharacterDetailController {

    private final CharacterDetailRepository characterDetailRepository;

    private final CharacterDetailResourceAssembler assembler;

    public CharacterDetailController(CharacterDetailRepository characterDetailRepository,
                                     CharacterDetailResourceAssembler assembler) {
        this.characterDetailRepository = characterDetailRepository;
        this.assembler = assembler;
    }


    @GetMapping("/character/{id}")
    public Resource<CharacterDetail> one(@PathVariable Long id) {

        CharacterDetail characterDetail = characterDetailRepository.findById(id)
                .orElseThrow(() -> new CharacterDetailNotFoundException(id));

        return assembler.toResource(characterDetail);
    }

    @PostMapping("/character")
    public ResponseEntity<?> newCharacterDetail(@RequestBody CharacterDetail newCharacterDetail) throws URISyntaxException {

        newCharacterDetail.setLevel(Level.BEGINNER);
        Resource<CharacterDetail> resource = assembler.toResource(characterDetailRepository.save(newCharacterDetail));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

}
