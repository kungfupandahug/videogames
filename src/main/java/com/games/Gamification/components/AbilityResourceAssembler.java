package com.games.Gamification.components;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.games.Gamification.controllers.AbilityController;
import com.games.Gamification.models.Ability;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class AbilityResourceAssembler implements ResourceAssembler<Ability, Resource<Ability>> {

    @Override
    public Resource<Ability> toResource(Ability ability) {

        return new Resource<>(ability,
                linkTo(methodOn(AbilityController.class).one(ability.getName())).withSelfRel());
    }
}
