package com.games.Gamification.components;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.games.Gamification.Level;
import com.games.Gamification.controllers.CharacterDetailController;
import com.games.Gamification.models.CharacterDetail;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class CharacterDetailResourceAssembler implements ResourceAssembler<CharacterDetail, Resource<CharacterDetail>> {

    @Override
    public Resource<CharacterDetail> toResource(CharacterDetail characterDetail) {

        return new Resource<>(characterDetail,
                linkTo(methodOn(CharacterDetailController.class).one(characterDetail.getId())).withSelfRel());
    }
}
