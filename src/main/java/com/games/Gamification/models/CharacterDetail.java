package com.games.Gamification.models;

import com.games.Gamification.Level;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "CHARACTER_DETAIL")
public class CharacterDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private Level level;

    @Column(name = "userid")
    private Long userid;

    @Column(name = "abilityid")
    private Long abilityid;

    public CharacterDetail() {}

    public CharacterDetail(String name, Level level) {
        this.name = name;
        this.level = level;
    }

}
