package com.games.Gamification.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "ABILITY")
public class Ability {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "abilityid")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "abilityid", referencedColumnName = "abilityid")
    private List<CharacterDetail> characters;

    public Ability() {
        characters = new ArrayList<CharacterDetail>();
    }

    public Ability(String name) {
        this();
        this.name = name;
    }

}
