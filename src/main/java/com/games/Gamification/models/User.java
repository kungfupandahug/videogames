package com.games.Gamification.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userid")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    private List<CharacterDetail> characters;

    public User() {
        characters = new ArrayList<CharacterDetail>();
    }

    public User(String username, String password) {
        this();
        this.username = username;
        this.password = password;
    }

}
