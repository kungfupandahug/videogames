package com.games.Gamification;

public enum Level {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED;
}
