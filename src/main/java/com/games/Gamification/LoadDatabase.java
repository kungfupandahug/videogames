package com.games.Gamification;

import com.games.Gamification.models.Ability;
import com.games.Gamification.repositories.AbilityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    public CommandLineRunner initDatabase(AbilityRepository abilityRepository) {

        return args -> {
            /* UNCOMMENTED AFTER INITIALIZATION
            abilityRepository.save(new Ability("Fighter"));
            abilityRepository.save(new Ability("Wizard"));
            abilityRepository.save(new Ability("Rogue"));
             */
        };
    }
}
